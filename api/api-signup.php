<?php

session_start();

  if( ! is_csrf_valid() ){
    sendError(400, 'Invalid CSRF token', __LINE__);
    exit();
  }

if (! isset($_POST['username'])){
  sendError(400, 'Something went wrong, Error:', __LINE__);
  }

  if( strlen($_POST['username']) < 8 ) {
    sendError(400, 'Username must be atleast 8 characters', __LINE__);
  }
  
  if( strlen($_POST['username']) > 75 ) {
    sendError(400, 'Username must be max 75 characters', __LINE__);
  }

  if (! isset($_POST['password'])){
    sendError(400, 'Something went wrong, Error:', __LINE__);
    }
  
  if( strlen($_POST['password']) < 8 ) {
    sendError(400, 'Password must be atleast 8 characters', __LINE__);
  }
  
  if( strlen($_POST['password']) > 75 ) {
    sendError(400, 'Password must be max 75 characters', __LINE__);
  }

  if (! isset($_POST['firstname'])){
    sendError(400, 'Something went wrong, Error:', __LINE__);
    }

  if( strlen($_POST['firstname']) < 2 ) {
    sendError(400, 'First name must be atleast 2 characters', __LINE__);
  }
  
  if( strlen($_POST['firstname']) > 20 ) {
    sendError(400, 'First name must be max 20 characters', __LINE__);
  }

  if (! isset($_POST['lastname'])){
    sendError(400, 'Something went wrong, Error:', __LINE__);
    }

  if( strlen($_POST['lastname']) < 2 ) {
    sendError(400, 'Last name must be atleast 2 characters', __LINE__);
  }
  
  if( strlen($_POST['lastname']) > 30 ) {
    sendError(400, 'Last name must be max 30 characters', __LINE__);
  }

  if (! isset($_POST['email'])){
    sendError(400, 'Something went wrong, Error:', __LINE__);
    }

  if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    sendError(400, 'Invalid email', __LINE__);
  }


//   if ($_FILES['file']['name'] !== "") {

//     $fileName = $_FILES['file']['name'];
//     $fileTmpName = $_FILES['file']['tmp_name'];
//     $fileSize = $_FILES['file']['size'];
//     $fileError = $_FILES['file']['error'];


//     $fileExt = explode('.', $fileName);
//     $fileActualExt = strtolower(end($fileExt));

//     $allowed = ['jpg', 'jpeg', 'png', 'gif'];

//     if (in_array($fileActualExt, $allowed)) {
//         if ($fileError === 0) {
//             // 500 kb
//             if ($fileSize < 500000) {
                
//                 // rename the image to a unique name
//                 $newFileName = uniqid('', true).".".$fileActualExt;

//                 // get the images folder
//                 $fileDestination = dirname(getcwd()).'/assets/avatars/'.$newFileName;

//                 // move the file from temporary location to /images/avatars/ directory
//                 move_uploaded_file($fileTmpName, $fileDestination);

//             } else {
//                 sendError(400, 'Your file size is too large. Avatar max size is 500 kb', __LINE__);
//             }
//         } else {
//             sendError(400, 'There was an error in uploading this file', __LINE__);
//         }
//     } else {
//         sendError(400, 'Only jpg, jpeg, png & gif are allowed', __LINE__);
//     }
// } else {
//     $newFileName = 'default.jpg';
// }
  
  require_once( __DIR__.'/../private/db.php' );

  try {

        // check if email exists

        $q = $db->prepare('SELECT * FROM Users WHERE user_email = :email LIMIT 1');
        $q->bindValue(':email', $_POST['email']);
        $q->execute();
    
        $aRow = $q->fetch();
        if ( $aRow ){
          sendError(500, 'email already registered', __LINE__);
        }
    
        // check if username exists
    
        $q = $db->prepare('SELECT * FROM Users WHERE user_username = :username LIMIT 1');
        $q->bindValue(':username', $_POST['username']);
        $q->execute();
    
        $aRow = $q->fetch();
        if ( $aRow ){
          sendError(500, 'username already exists', __LINE__);
        }


        //image 
          $image_temp;
          if(isset($_FILES['image-uploaded']) && $_FILES['image-uploaded']['size'] > 0){
            $image = $_FILES['image-uploaded']['name'];
            $post_image_path = uniqid().'.jpg';
            $image_temp = $_FILES['image-uploaded']['tmp_name'];
            $target=__DIR__.'/../assets/userProfilePictures/'.$post_image_path;
            move_uploaded_file($image_temp, "$target");
        } else 
          $post_image_path = 'default.jpg';

        // make query
    
        $q = $db->prepare('INSERT INTO Users
        VALUES(:iId, :sUserName, :sName, :sLastName, :sEmail, :sPassword, :sAvatar, :iAdmin )');
        $q->bindValue(':iId', null);
        $q->bindValue(':sName', $_POST['firstname']);
        $q->bindValue(':sLastName', $_POST['lastname']);
        $q->bindValue(':sEmail', $_POST['email']);
        $q->bindValue(':sUserName', $_POST['username']);  
        $q->bindValue(':sPassword', password_hash($_POST['password'], PASSWORD_DEFAULT));
        $q->bindValue(':sAvatar', $post_image_path);
        $q->bindValue(':iAdmin', 0);

        $q->execute();

        http_response_code(200);
        header('Content-Type: application/json');
        echo '{"message":"Success!"}';
    

  }
  catch (Exception $ex) {
        echo $ex;
        sendError(500, 'error in signup', __LINE__);
  }

  // #################
  // #################
  // #################

  function sendError($iResponseCode, $sMessage, $iLine){
    http_response_code($iResponseCode);
    header('Content-Type: application/json');
    echo '{"message":"'.$sMessage.'", "error":' .$iLine.'}';
    exit();
  }