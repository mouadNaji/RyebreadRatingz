<?php

session_start();

if (! isset($_SESSION['id'])){
  sendError(400, 'Something went wrong, Error:', __LINE__);
}
if(! ctype_digit($_SESSION['id']) ){
  sendError(400, 'Something went wrong, Error:', __LINE__);
}

if (! isset($_SESSION['useradmin'])){
  sendError(400, 'Something went wrong, Error:', __LINE__);
}
if(! ctype_digit($_SESSION['useradmin']) ){
  sendError(400, 'Something went wrong, Error:', __LINE__);
}

if ( $_SESSION['useradmin'] === 0){
  sendError(400, 'Something went wrong, Error:', __LINE__);
}


  if( ! is_csrf_valid() ){
    sendError(400, 'Invalid CSRF token', __LINE__);
    exit();
  }

if (! isset($_POST['breaddesc'])){
  sendError(400, 'Something went wrong, Error:', __LINE__);
  }

if (! isset($_POST['breadname'])){
  sendError(400, 'Something went wrong, Error:', __LINE__);
  }
if (! isset($_POST['breadprice'])){
  sendError(400, 'Something went wrong, Error:', __LINE__);
  }

if (! isset($_POST['breadbrand'])){
  sendError(400, 'Something went wrong, Error:', __LINE__);
  }


 
  require_once( __DIR__.'/../private/db.php' );

  try {
        //image 
          $image_temp;
          if(isset($_FILES['image-uploaded']) && $_FILES['image-uploaded']['size'] > 0){
            $image = $_FILES['image-uploaded']['name'];
            $post_image_path = uniqid().'.jpg';
            $image_temp = $_FILES['image-uploaded']['tmp_name'];
            $target=__DIR__.'/../assets/'.$post_image_path;
            move_uploaded_file($image_temp, "$target");
        } else {
          sendError(400, 'No image uploaded', __LINE__);
        }
        // make query
        $q = $db->prepare('INSERT INTO Posts
        VALUES(:iId, :Userfk, :img, :breaddesc, :breadname, :breadprice, :breadbrand, :bredtimestamp )');
        $q->bindValue(':iId', null);
        $q->bindValue(':Userfk', $_SESSION['id']);
        $q->bindValue(':img', $post_image_path);
        $q->bindValue(':breaddesc', $_POST['breaddesc']);
        $q->bindValue(':breadname', $_POST['breadname']);
        $q->bindValue(':breadprice', $_POST['breadprice']);  
        $q->bindValue(':breadbrand', $_POST['breadbrand']);
        $q->bindValue(':bredtimestamp', null);
        $q->execute();

        http_response_code(200);
        header('Content-Type: application/json');
        echo '{"message":"Success!"}';

  }
  catch (Exception $ex) {
        echo $ex;
        sendError(500, 'error', __LINE__);
  }

  // #################
  // #################
  // #################

  function sendError($iResponseCode, $sMessage, $iLine){
    http_response_code($iResponseCode);
    header('Content-Type: application/json');
    echo '{"message":"'.$sMessage.'", "error":' .$iLine.'}';
    exit();
  }