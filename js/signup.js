async function signup() {
  const error = document.querySelector(".error-message");
  const attempts = document.querySelector(".attempts");
  const time = document.querySelector(".time");
  error.textContent = "";
  attempts.textContent = "";
  time.textContent = "";

  var connection = await fetch("api-signup", {
    method: "POST",
    body: new FormData(event.target)
  });

  var sResponse = await connection.json();

  error.append(sResponse.message);

  // if (sResponse.attempts) {
  //   attempts.append(`Attempts: ${sResponse.attempts}`);
  // }

  // if (sResponse.time_passed) {
  //   const time_passed = new Date(sResponse.time_passed * 1000).toISOString().substr(11, 8);
  //   time.append(`Time passed: ${time_passed}`);
  // }

  if (connection.status == 200) {
    location.href = "/login";
  }
}
