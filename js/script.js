// get bread list
async function getPosts() {
  document.querySelector("#posts").innerHTML = "";
  let bridge = await fetch("api-get-all-posts");
  let text = await bridge.text();

  let bread = JSON.parse(text);
  console.log(bread[0].bread_name);

  for (i = 0; i < bread.length; i++) {
    let indhold = `<div><a href="/bread/${bread[i].id}"><h1>${bread[i].bread_name} </h1><img class="bread-img" src="assets/${bread[i].post_img}"></a> </div>`;
    document.querySelector("#posts").insertAdjacentHTML("beforeend", indhold);
  }
}
getPosts();

async function postBread() {
  const error = document.querySelector(".error-message");
  const attempts = document.querySelector(".attempts");
  const time = document.querySelector(".time");
  error.textContent = "";
  attempts.textContent = "";
  time.textContent = "";

  var connection = await fetch("api-post-bread", {
    method: "POST",
    body: new FormData(event.target),
  });

  var sResponse = await connection.json();

  error.append(sResponse.message);

  if (connection.status == 200) {
    console.log("succes");
    getPosts();
    document.querySelector("#form").reset();
  }
}
