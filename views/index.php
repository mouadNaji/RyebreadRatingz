<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Home:: RyebreadRatingz</title>
    <meta name="robots" content="noindex" />
    <link rel="stylesheet" href="/../css/styles.css" />
    <link rel="shortcut icon" href="/../assets/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="./assets/favicon.ico" type="image/x-icon" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Cherry+Swash:wght@400;700&display=swap" rel="stylesheet">
  </head>
  <body>

  <?php

  session_start();
  if(! $_SESSION["id"]){
    header("location: login");
  }

  if(! ctype_digit($_SESSION['id']) ){
    sendError(400, 'Something went wrong, Error:', __LINE__);
  }  
 

  ?>

    <header>
        <h1>RyebreadRatingz.com</h1>
    </header>

    <p>Welcome, <?=$_SESSION['username'];?></p> 


   
    <?if($_SESSION["useradmin"] == 1){?>
    <div id="admin">
    <h1>admin zone</h1>
    <div>
    <h2>Post a ryebread</h2>
    <form id="form" onsubmit="postBread(); return false;">
        <?php set_csrf() ?>
        <input type="text" name="breadname" placeholder="Bread name">
        <input type="number" name="breadprice" placeholder="Bread price">
        <textarea style="resize: none;" name="breaddesc" placeholder="Bread description" cols="10" rows="5"></textarea>
        <input type="text" name="breadbrand" placeholder="Bread brand">
        
        <label for="image-uploaded" >Bread image:</label>
        <input type="file" name="image-uploaded">
        
        <button>POST</button>
    </form>
    
    <div id="error-message">
        <p class="error-message"></p>
        <p class="attempts"></p>
        <p class="time"></p>
    </div>
    
    
    
    </div>

     <? 
    }
    ?>
    
    </div>

    <div id="posts"></div>

    
    <script src="/../js/script.js"></script>




    <div>


    <a href="logout">Log Out</a>

    </div>

  </body>
</html>
