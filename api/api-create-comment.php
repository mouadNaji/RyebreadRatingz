<?php
session_start();

if (! isset($_SESSION['id'])){
    sendError(400, 'Something went wrong, Error:', __LINE__);
}
if(! ctype_digit($_SESSION['id']) ){
    sendError(400, 'Something went wrong, Error:', __LINE__);
}

if (! isset($_POST['text'])){
    sendError(400, 'Something went wrong, Error:', __LINE__);
}

if (strlen($_POST['text']) < 1){
    sendError(400, 'Something went wrong, Error:', __LINE__);
}

if (strlen($_POST['text']) > 500){
    sendError(400, 'Something went wrong, Error:', __LINE__);
}

if (! isset($_POST['postId'])){
    sendError(400, 'Something went wrong, Error:', __LINE__);
}

if(! ctype_digit($_POST['postId']) ){
    sendError(400, 'Something went wrong, Error:', __LINE__);
}

try{
session_start();
$userId = $_SESSION['id'];
$comment = $_POST['text'];
$postId = $_POST['postId'];
require_once(__DIR__.'/../private/db.php');

//TODO: Validate
$query = $db->prepare("INSERT INTO Comments VALUES (NULL, :userId, :postId, :comment, NULL);");
$query->bindValue(':comment', $comment);
$query->bindValue(':postId', $postId);
$query->bindValue(':userId', $userId);

$query->execute();
$id = $db->lastInsertId();

http_response_code(200);
header("content-type: application/JSON");
echo '{"message":'.$id.'}';
}catch(PDOException $ex){
    sendError('system under maintenance',__LINE__,500);
}

// ##############################################################
// ##############################################################
// ##############################################################
// ##############################################################
function sendError($sMessage, $iLine, $iErrorCode){
    http_response_code($iErrorCode);
    header('content-type: application/json');
    echo '{"message":"'.$sMessage.'", "error":"'.$iLine.'"}';
    exit();
}











