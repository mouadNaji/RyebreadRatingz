<?php

session_start();


if( !is_csrf_valid() ){
  sendError(400, 'Invalid CSRF token', __LINE__);
  exit();
}


if( strlen($_POST['username']) < 8 ) {
  sendError(400, 'Username must be atleast 8 characters', __LINE__);
}

if( strlen($_POST['username']) > 75 ) {
  sendError(400, 'Username must be max 75 characters', __LINE__);
}

if( strlen($_POST['password']) < 8 ) {
  sendError(400, 'Password must be atleast 8 characters', __LINE__);
}

if( strlen($_POST['password']) > 75 ) {
  sendError(400, 'Password must be max 75 characters', __LINE__);
}

require_once( __DIR__.'/../private/db.php' );


try {

  // check if ip exists in IPBans table

  $q = $db->prepare('SELECT * FROM IPBans WHERE ip_address = :ip_address LIMIT 1');
  $q->bindValue(':ip_address', getIpAddr());
  $q->execute();
  $row_from_ipbans = $q->fetch();

  // if the ip is banned check to see how old the ban is

  if ($row_from_ipbans) {

    // echo "the ip exists in IPBans table";

    // check timestamp

    $current_time = time();

      if (600 > $current_time - $row_from_ipbans->timestamp) {

        $time_passed = $current_time - $row_from_ipbans->timestamp;

        http_response_code(400);
        header('Content-Type: application/json');
        echo '{"message":"Your IP has been suspended for 10 minutes - try again later", "time_passed":'.$time_passed.'}';
        exit();
      }

      if (600 < $current_time - $row_from_ipbans->timestamp) {

        // remove ip from IP Bans table
        $q = $db->prepare('DELETE FROM IPBans WHERE ip_address = :ip_address');
        $q->bindValue(':ip_address', getIpAddr());
        $q->execute();

        // echo 'lets login';

        $q = $db->prepare('SELECT * FROM LoginAttempts WHERE ip_address = :ip_address LIMIT 1');
        $q->bindValue(':ip_address', getIpAddr());
        $q->execute();
        $row_from_attempts = $q->fetch();
    
        $current_login_attempts = $row_from_attempts->login_attempts ?? 0;
        $attempts_to_frontend = $current_login_attempts + 1;
    
          // if not exists in login_attempts table
    
          if (!$row_from_attempts) {
            $q = $db->prepare('INSERT INTO LoginAttempts VALUES(:id, :ip_address, :login_attempt, :epoch)');
            $q->bindValue(':id', null);
            $q->bindValue(':ip_address', getIpAddr());
            $q->bindValue(':login_attempt', 1);
            $q->bindValue(':epoch', time());
            $q->execute();
          }
    
          if ($current_login_attempts > 2) {
    
            $q = $db->prepare('INSERT INTO IPBans VALUES(:id, :ip_address, :epoch)');
            $q->bindValue(':id', null);
            $q->bindValue(':ip_address', getIpAddr());
            $q->bindValue(':epoch', time());
            $q->execute();
    
            // echo 'delete row from attempts table';

            $q = $db->prepare('DELETE FROM LoginAttempts WHERE ip_address = :ip_address');
            $q->bindValue(':ip_address', getIpAddr());
            $q->execute();
    
            http_response_code(400);
            header('Content-Type: application/json');
            echo '{"message":"Your IP is suspended for 10 minutes - try again later", "time_passed": '.$time_passed.'}';
            exit();
            
          }
    
          if ($current_login_attempts <= 2) {
    
            // echo 'ready to attempt login...';
    
            $q = $db->prepare('SELECT * FROM Users WHERE user_username = :username LIMIT 1');
            $q->bindValue(':username', $_POST['username']);
            // $q->bindValue(':password', $_POST['password']);
            $q->execute();
            $row = $q->fetch();
    
            // if username doesnt exist OR password does not verify
            if (!$row || !password_verify($_POST['password'], $row->user_password)) {
              
              $q = $db->prepare('UPDATE LoginAttempts SET `login_attempts` = :attempts WHERE ip_address = :ip_address');
              $q->bindValue(':ip_address', getIpAddr());
              $q->bindValue(':attempts', $current_login_attempts + 1);
              $q->execute();
    
              http_response_code(400);
              header('Content-Type: application/json');
              echo '{"attempts":"'.$attempts_to_frontend.'", "message":"Login failed – wrong credentials"}';
              exit();
    
            }
    
            // if login succeeds
            // if ($row && password_verify($_POST['password'], $row->user_password)) {
    
            //   $q = $db->prepare('DELETE FROM LoginAttempts WHERE ip_address = :ip_address');
            //   $q->bindValue(':ip_address', getIpAddr());
            //   $q->execute();
    
            //   session_start();

            //   $_SESSION['id'] = $row->user_id;
            //   $_SESSION['username'] = $row->user_name;
    
            //   http_response_code(200);
            //   echo 'jshfkjsdfglsfs';
    
            //   // redirect to index.php

            //   header('location: /');
    
            //   exit();
            // }
    
          }

      }

  }


  // if ip NOT exists in IPBans table

  if (!$row_from_ipbans) {

    // echo "the ip NOT exists in IPBans table";

    $q = $db->prepare('SELECT * FROM LoginAttempts WHERE ip_address = :ip_address LIMIT 1');
    $q->bindValue(':ip_address', getIpAddr());
    $q->execute();
    $row_from_attempts = $q->fetch();

    $current_login_attempts = $row_from_attempts->login_attempts ?? 0;
    $attempts_to_frontend = $current_login_attempts + 1;

      // if not exists in login_attempts table

      if (!$row_from_attempts) {
        $q = $db->prepare('INSERT INTO LoginAttempts VALUES(:id, :ip_address, :login_attempt, :epoch)');
        $q->bindValue(':id', null);
        $q->bindValue(':ip_address', getIpAddr());
        $q->bindValue(':login_attempt', 1);
        $q->bindValue(':epoch', time());
        $q->execute();
        // echo 'inserted into attempts...';
      }

      if ($current_login_attempts > 2) {

        $q = $db->prepare('INSERT INTO IPBans VALUES(:id, :ip_address, :epoch)');
        $q->bindValue(':id', null);
        $q->bindValue(':ip_address', getIpAddr());
        $q->bindValue(':epoch', time());
        $q->execute();

        // echo 'delete row from attempts table';
        $q = $db->prepare('DELETE FROM LoginAttempts WHERE ip_address = :ip_address');
        $q->bindValue(':ip_address', getIpAddr());
        $q->execute();

        http_response_code(400);
        header('Content-Type: application/json');
        echo '{"message":"IP suspended - try again in 10 minutes"}';
        exit();
        
      }

      if ($current_login_attempts <= 2) {

        // echo 'ready to attempt login...';

        $q = $db->prepare('SELECT * FROM Users WHERE user_username = :username LIMIT 1');
        $q->bindValue(':username', $_POST['username']);
        // $q->bindValue(':password', $_POST['password']);
        $q->execute();
        $row = $q->fetch();

        // if username doesnt exist OR password is wrong
        if (!$row || !password_verify($_POST['password'], $row->user_password)) {
          
          $q = $db->prepare('UPDATE LoginAttempts SET `login_attempts` = :attempts WHERE ip_address = :ip_address');
          $q->bindValue(':ip_address', getIpAddr());
          $q->bindValue(':attempts', $current_login_attempts + 1);
          $q->execute();

          http_response_code(400);
          header('Content-Type: application/json');
          echo '{"attempts":"'.$attempts_to_frontend.'", "message":"Login failed – wrong credentials"}';
          exit();

        }

        // if username is found AND password verifies
        if ($row && password_verify($_POST['password'], $row->user_password)) {

          $q = $db->prepare('DELETE FROM LoginAttempts WHERE ip_address = :ip_address');
          $q->bindValue(':ip_address', getIpAddr());
          $q->execute();

          // session_start();
          $_SESSION['id'] = $row->user_id;
          $_SESSION['username'] = $row->user_name;
          $_SESSION['useradmin'] = $row->user_admin;

          http_response_code(200);
          echo '{"message":"Success"}';

          // redirect to index.php

          exit();
        }

      }

  }



} catch (Exception $ex) {
    // sendError(400, 'system under maintainance', __LINE__);
    echo $ex;
}

// #############################################

function sendError($iResponseCode, $sMessage, $iLine){
    http_response_code($iResponseCode);
    header('Content-Type: application/json');
    echo '{"message":"'.$sMessage.'", "error":' .$iLine.'}';
    exit();
  }

// #############################################

function getIpAddr(){

  if (!empty($_SERVER['HTTP_CLIENT_IP'])){
      $ipAddr=$_SERVER['HTTP_CLIENT_IP'];

  } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
      $ipAddr=$_SERVER['HTTP_X_FORWARDED_FOR'];

  } else {
      $ipAddr=$_SERVER['REMOTE_ADDR'];
  }
      return $ipAddr;
  }