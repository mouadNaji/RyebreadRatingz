<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/../css/styles.css" />
    <meta name="robots" content="noindex" />
    <title>Bread Detail :: RyebreadRatingz</title>
    <link rel="shortcut icon" href="/../assets/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="./assets/favicon.ico" type="image/x-icon" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Cherry+Swash:wght@400;700&display=swap" rel="stylesheet">
</head>
<body>

<?php
     session_start();

    if(! $_SESSION["id"]){
        header("location: ../login");
    }

    if(! ctype_digit($_SESSION['id']) ){
        sendError(400, 'Something went wrong, Error:', __LINE__);
    }    

    try{
    require_once(__DIR__.'/../private/db.php');
    $query = $db->prepare('SELECT * FROM Posts WHERE id=:id ');
    $query->bindValue(':id', $id);
    $query->execute();
    $data = $query->fetch();
    http_response_code(200);

    if(!$data){
        header("location: ../404");
    }

    }catch(PDOException $ex){
        sendError('system under maintainance',__LINE__,500);
    }

    ##############################################################
    function sendError($sMessage, $iLine, $iErrorCode){
        http_response_code($iErrorCode);
        header('content-type: application/json');
        echo '{"message":"'.$sMessage.'", "error":"'.$iLine.'"}';
        exit();
        }
    ?>

    <a href="/">Back to home page</a>
    <div class="container">
        
        <div class="bread-info">
            <h2><?= out($data->bread_name)?></h2>
            <p><?= out($data->bread_description);?></p>
            <p><?= out($data->bread_price);?>kr</p>
            <p><?= out($data->bread_brand);?></p>
            <img class="bread-img" src="../assets/<?=out($data->post_img);?>" alt="">
            <form id="form" onsubmit="comment(<?=out($id)?>); return false;">
            <input type="text" name="text" placeholder="comment">
            <button>Comment</button>
            </form>
            <div id="comments">
            </div>
            
        </div>
    
    




<script src="/../js/comment.js"></script>
    </body>
</html>



