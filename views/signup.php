<?php session_start() ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="noindex" />
    <link rel="stylesheet" href="/../css/styles.css" />
    <link rel="shortcut icon" href="/../assets/favicon.ico" type="image/x-icon" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Cherry+Swash:wght@400;700&display=swap" rel="stylesheet">
    <title>Sign up :: RyebreadRatingz</title>
</head>
<body>

    <header>
        <h1>RyebreadRatingz.com</h1>
    </header>

    <form id="form" onsubmit="signup(); return false;">
        <?php set_csrf() ?>
        <input type="text" name="username" placeholder="Username">
        <input type="text" name="firstname" placeholder="First Name">
        <input type="text" name="lastname" placeholder="Last Name">
        <input type="text" name="email" placeholder="Email">
        <input type="password" name="password" placeholder="Password">
        <label for="image-uploaded" >Profile image:</label>
        <input type="file" name="image-uploaded">
        
        <button>SIGN UP</button>
    </form>

    <div id="error-message">
        <p class="error-message"></p>
        <p class="attempts"></p>
        <p class="time"></p>
    </div>

    <a href="login">Already have a account? Login in here</a>

<script src="/../js/signup.js"></script>
</body>
</html>