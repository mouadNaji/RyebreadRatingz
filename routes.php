<?php

require_once("{$_SERVER['DOCUMENT_ROOT']}/router.php");

get('/', 'views/index.php');
get('/login', 'views/login.php');
get('/signup', 'views/signup.php');
get('/bread/$id', 'views/bread-detail.php');
get('/logout','views/logout.php');

// APIS

get('/api-get-all-posts', 'api/api-get-all-posts.php');
get('/api-read-all-users', 'api/read-all-users.php');
post('/api-login', 'api/api-login.php');
post('/api-signup', 'api/api-signup.php');
post('/api-create-comment', 'api/api-create-comment.php');
post('/api-post-bread', 'api/api-post-bread.php');
get('/api-get-comments', 'api/api-get-comments.php');




//  this route must be at the bottom of the file
any('/404','views/404.php');