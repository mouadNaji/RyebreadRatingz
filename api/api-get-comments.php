<?php
session_start();


if (! isset($_SESSION['id'])){
    sendError(400, 'Something went wrong, Error:', __LINE__);
}
if(! ctype_digit($_SESSION['id']) ){
    sendError(400, 'Something went wrong, Error:', __LINE__);
}


try{
require_once(__DIR__.'/../private/db.php');

$query = $db->prepare('SELECT * FROM `Comments` JOIN Users ON Users.user_id=Comments.user_fk');
$query->execute();
$aRows =$query->fetchAll();

http_response_code(200);
header("content-type: application/JSON");
echo json_encode($aRows);
exit();

}catch(PDOException $ex){
    sendError('system under maintainance',__LINE__,500);
}












// ##############################################################
// ##############################################################
// ##############################################################
// ##############################################################
function sendError($sMessage, $iLine, $iErrorCode){
    http_response_code($iErrorCode);
    header('content-type: application/json');
    echo '{"message":"'.$sMessage.'", "error":"'.$iLine.'"}';

    exit();
}

