async function comment(id) {
  form = new FormData(document.querySelector("#form"));
  form.append("postId", id);
  let bridge = await fetch("/api-create-comment", {
    method: "POST",
    body: form,
  });
  if (bridge.status == "200") {
    console.log("commented");
    getComments();
    return;
  } else {
    alert("error");
  }
}

async function getComments() {
  document.querySelector("#comments").innerHTML = "";
  const lastItem = window.location.href.substring(window.location.href.lastIndexOf("/") + 1);
  let bridge = await fetch("/api-get-comments");
  let text = await bridge.text();

  let comments = JSON.parse(text);

  for (i = 0; i < comments.length; i++) {
    if (lastItem == comments[i].post_fk) {
      let indhold = `<div style="margin: 1rem; padding:0.5rem; background:wheat; text-align:center;" id="${comments[i].id}"><img style="width:3rem" src="../assets/userProfilePictures/${comments[i].user_img}" alt=""><h3>from: ${comments[i].user_name}</h3><p>comment: ${comments[i].text}</p> </div>`;
      document.querySelector("#comments").insertAdjacentHTML("afterbegin", indhold);
    }
  }
}

getComments();
// const lastItem = window.location.href.substring(
//   window.location.href.lastIndexOf("/") + 1
// );
// alert(lastItem);
